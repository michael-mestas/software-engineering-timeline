"""
Definition of urls for SoftwareEngeneeringTimeLine.
"""
from django.conf.urls import url

from django.conf.urls import include
from django.contrib import admin

from aop.urls import urlpatterns
import app.views

admin.autodiscover()

urlpatterns = [
    url(r'^$', app.views.home, name='home'),
    url(r'^timeline/$', app.views.timeline, name='timeline'),
    url(r'^aop/', include('aop.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
