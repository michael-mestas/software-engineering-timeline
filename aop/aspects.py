from symbol import decorator

import aspectlib


def p_decorate(func):
    def func_wrapper(name):

        return "ASPECT BEFORE "+(func(name))
    return func_wrapper


def q_decorate(func):
    def wrapper_func(name):

        return func(name)+" ASPECT AFTER"
    return wrapper_func

