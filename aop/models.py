from django.db import models


class Box(models.Model):

    point_one = models.FloatField()
    point_two = models.FloatField()
    point_three = models.FloatField()
    point_four = models.FloatField()


class Pokemon(models.Model):

    name = models.CharField(max_length=64)
    type = models.CharField(max_length=32)
    type_two = models.CharField(max_length=32)
