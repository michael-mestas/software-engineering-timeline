from django.conf.urls import url

from aop.views import index, example

urlpatterns = [
    url(r'^tutorial/$', example, name='tutorial'),
    url(r'^$', index, name='home')
]