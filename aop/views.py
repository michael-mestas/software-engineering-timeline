from aspectlib import weave
from django.shortcuts import render, render_to_response

from aop.aspects import p_decorate, q_decorate
from aop.models import Pokemon


def index(request):
    context = {}
    print(sum_a(1))

    print(sum_b(1))
    # patch = weave(sum_a, print_message_aspect())
    return render(request, 'app/main.html', context)


def example(request):
    """pieChart page"""

    xdata = ["Apple", "Apricot", "Avocado", "Banana", "Boysenberries",
             "Blueberries", "Dates", "Grapefruit", "Kiwi", "Lemon"]
    ydata = [52, 48, 160, 94, 75, 71, 490, 82, 46, 17]

    color_list = ['#5d8aa8', '#e32636', '#efdecd', '#ffbf00', '#ff033e', '#a4c639',
                  '#b2beb5', '#8db600', '#7fffd4', '#ff007f', '#ff55a3', '#5f9ea0']
    extra_serie = {
        "tooltip": {"y_start": "", "y_end": " cal"},
        "color_list": color_list
    }
    chartdata = {'x': xdata, 'y1': ydata, 'extra1': extra_serie}
    charttype = "pieChart"
    chartcontainer = 'piechart_container'  # container name

    data = {
        'charttype': charttype,
        'chartdata': chartdata,
        'chartcontainer': chartcontainer,
        'extra': {
            'x_is_date': False,
            'x_axis_format': '',
            'tag_script_js': True,
            'jquery_on_ready': False,
        }
    }
    return render_to_response('app/tutorial.html', data)

def play_count_by_month(request):
    import json
    import csv
    from pprint import pprint

    # Now, we define a dictionary to store the result
    typePokemon = {}

    # Open and load the JSON file.
    with open("pokemon.json") as f:
        data = json.loads(f.read())

    # Fill the typePokemon dictionary with sum of pokemon by type
    for line in data:
        if line["type"] not in typePokemon:
            typePokemon[line["type"]] = 1
        else:
            typePokemon[line["type"]] = typePokemon.get(line["type"]) + 1

    # Open in a write mode the sumPokemon.csv file
    with open("sumPokemon.csv", "w") as a:
        w = csv.writer(a)

    # Sort the dictionary by number of pokemon
    # writes the result (type and amount) into the csv file
    for key, value in sorted(typePokemon.items(),key=lambda x: x[1]):
        w.writerow([key, str(value)])

    # finally, we use "pretty print" to print the dictionary
    pprint(typePokemon)


#     data = Pokemon.objects.all() \
#         .extra(select={'month': connections[Play.objects.db].ops.date_trunc_sql('month', 'date')}) \
#         .values('month') \
#         .annotate(count_items=Count('id'))
#     return JsonResponse(list(data), safe=False)

@p_decorate
def sum_a(a):
    return "sum was made"


@q_decorate
def sum_b(a):
    return "sum was made"
